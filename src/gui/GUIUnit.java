package gui;

import logic.LogicUnit;

import javax.swing.*;

/**
 * Superclass for all UI parts.
 */
public class GUIUnit extends JPanel {
    protected final LogicUnit logic;

    public GUIUnit(LogicUnit logic) {
        this.logic = logic;
    }
}
