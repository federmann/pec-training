package gui;

import logic.menu.Menu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;

/**
 * UI for main menu.
 */
public class MenuPanel extends GUIUnit {

    public MenuPanel(Menu logic) {
        super(logic);
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        setBackground(new Color(0, 81, 0));
        JPanel panelNav = new JPanel();
        panelNav.setLayout(new BoxLayout(panelNav, BoxLayout.Y_AXIS));
        panelNav.setOpaque(false);

        Button buttonExit = new Button("Schließen"), buttonRun = new Button("Starten");

        buttonExit.addActionListener((ActionEvent e) -> logic.exit());
        buttonExit.setAlignmentX(CENTER_ALIGNMENT);
        buttonRun.addActionListener((ActionEvent e) -> logic.startGame());
        buttonRun.setFont(buttonRun.getFont().deriveFont(80.0f));
        buttonRun.setAlignmentX(CENTER_ALIGNMENT);

        // logo
        ImageIcon icon = new ImageIcon(getClass().getResource("/logo.jpg"));
        BufferedImage in = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics g = in.createGraphics();
        // paint the Icon to the BufferedImage.
        icon.paintIcon(null, g, 0,0);
        g.dispose();
        icon = new ImageIcon(new ImageIcon(in).getImage().getScaledInstance(200, 200, 0));
        JLabel logo = new JLabel(icon);
        logo.setAlignmentX(CENTER_ALIGNMENT);

        // game name
        JLabel title = new JLabel("PEC Training");
        title.setAlignmentX(CENTER_ALIGNMENT);
        title.setFont(title.getFont().deriveFont(150.0f));
        title.setForeground(new Color(229, 229, 229));

        JPanel panelTitle = new JPanel();
        panelTitle.setLayout(new BoxLayout(panelTitle, BoxLayout.X_AXIS));
        panelTitle.setOpaque(false);

        panelTitle.add(logo);
        panelTitle.add(Box.createHorizontalStrut(30));
        panelTitle.add(title);

        panelNav.add(Box.createVerticalGlue());
        panelNav.add(panelTitle);
        panelNav.add(Box.createVerticalGlue());
        panelNav.add(buttonRun);
        panelNav.add(Box.createVerticalStrut(40));
        panelNav.add(buttonExit);
        panelNav.add(Box.createVerticalGlue());
        panelNav.add(Box.createVerticalStrut(logo.getMaximumSize().height));

        add(panelNav);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g); // paint the background image and scale it to fill the entire space
        ImageIcon icon = new ImageIcon(getClass().getResource("/background1.jpg"));
        BufferedImage in = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics gr = in.createGraphics();
        // paint the Icon to the BufferedImage.
        icon.paintIcon(null, gr, 0,0);
        gr.dispose();
        icon = new ImageIcon(new ImageIcon(in).getImage().getScaledInstance(getWidth(), getHeight(), 0));
        g.drawImage(icon.getImage(), 0, 0, null);
    }
}
