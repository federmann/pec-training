package gui;

import logic.game.VisualScan;
import logic.menu.VisualScanMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;

/**
 * UI for menu of game visual scan.
 */
public class VisualScanMenuPanel extends GUIUnit {
    private static int selectedSymbol = 0; // saves the state of the symbol selection combobox
    private static int selectedLevel = 0; // saves the state of the level selection combobox

    public VisualScanMenuPanel(VisualScanMenu logic) {
        super(logic);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBackground(new Color(0, 81, 0));

        // input of symbols
        JComboBox<VisualScan.Symbols> symbolSelection = new JComboBox<>(VisualScan.Symbols.values());
        symbolSelection.setFont(new Font("SansSerif", Font.PLAIN, 38));
        symbolSelection.setSelectedIndex(selectedSymbol);
        symbolSelection.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    selectedSymbol = symbolSelection.getSelectedIndex();
                }
            }
        });

        // input of level
        JComboBox<String> levelSelection = new JComboBox<>(new String[]{"Level 1", "Level 2"});
        levelSelection.setFont(new Font("SansSerif", Font.PLAIN, 38));
        levelSelection.setSelectedIndex(selectedLevel);
        levelSelection.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    selectedLevel = levelSelection.getSelectedIndex();
                }
            }
        });

        JPanel panelSettings = new JPanel();
        panelSettings.setOpaque(false);
        panelSettings.add(symbolSelection);
        panelSettings.add(levelSelection);

        JLabel description = new JLabel("<html><p align=center style=\"margin-left: 6cm; margin-right: 6cm\">Ziel des Tests ist es, so schnell wie möglich alle Kopien des vorgegebenen Zeichens zu finden."
        		+ " Das zu suchende Zeichen wird links oben am Bildschirm angezeigt."
        		+ " Die Anzahl, wie oft das Zeichen auf dem Spielfeld noch vorhanden ist, wird oben rechts angezeigt."
        		+ " Daneben ist die verstrichene Zeit zu sehen.</p></html>");
        description.setAlignmentX(CENTER_ALIGNMENT);
        description.setFont(description.getFont().deriveFont(40.0f));
        description.setForeground(new Color(255, 255, 255));
        
        JPanel panelNavigation = new JPanel();
        panelNavigation.setOpaque(false);
        Button buttonBack = new Button("Zurück");
        buttonBack.addActionListener((ActionEvent e) -> logic.back());
        buttonBack.setForeground(Color.black);
        Button buttonStart = new Button("Start");
        buttonStart.setForeground(Color.black);
        buttonStart.addActionListener((ActionEvent e) -> logic.start((VisualScan.Symbols) symbolSelection.getSelectedItem(), levelSelection.getSelectedIndex() + 1));

        panelNavigation.add(buttonBack);
        panelNavigation.add(buttonStart);

        add(Box.createVerticalGlue());
        add(panelSettings);
        add(description);
        add(Box.createVerticalGlue());
        add(panelNavigation);
        add(Box.createVerticalGlue());
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g); // paint the background image and scale it to fill the entire space
        ImageIcon icon = new ImageIcon(getClass().getResource("/background2.jpg"));
        BufferedImage in = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics gr = in.createGraphics();
        icon.paintIcon(null, gr, 0,0);
        gr.dispose();
        icon = new ImageIcon(new ImageIcon(in).getImage().getScaledInstance(getWidth(), getHeight(), 0));
        g.drawImage(icon.getImage(), 0, 0, null);
    }
}
