package gui;

import logic.game.VisualScan;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.function.UnaryOperator;

/**
 * UI for game visual scan.
 */
public class VisualScanPanel extends GUIUnit{

    /**
     * Content for each tile in search field.
     */
    private class ContentLabel extends JLabel {
        /**
         * x-coordinate.
         */
        private final int x;
        /**
         * y-coordinate.
         */
        private final int y;
        private final VisualScan vs;

        /**
         * Creates a ContentLabel.
         * @param label String content of label.
         * @param vs    Logic for the ui.
         * @param x     x-coordinate of the label.
         * @param y     y-coordinate of the label.
         */
        private ContentLabel(String label, VisualScan vs, int x, int y) {
            super(label, SwingConstants.CENTER);
            this.vs = vs;
            this.x = x;
            this.y = y;
            initMouseListener();
            setFont(new Font("SansSerif", Font.BOLD, fontSize));
        }

        private void initMouseListener() {
            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent mouseEvent) {
                    super.mouseClicked(mouseEvent);
                    vs.click(x, y);
                }
            });
        }
    }

    /**
     * Label showing symbol to search.
     */
    private final JLabel labelSearch;
    /**
     * Label showing number of missing symbols.
     */
    private final JLabel labelMissing;
    /**
     * Label to display game time
     */
    private final JLabel labelTime;
    /**
     * Panel for search field.
     */
    private final JPanel contentPanel;
    /**
     * Font size for search field.
     */
    private int fontSize;
    private ContentLabel[][] content;

    public VisualScanPanel(VisualScan logic) {
        super(logic);
        JPanel topPanel = new JPanel();

        // init labels
        labelMissing = new JLabel();
        labelSearch = new JLabel();
        labelTime = new JLabel();
        labelMissing.setFont(new Font("SansSerif", Font.BOLD, 38));
        labelSearch.setFont(new Font("SansSerif", Font.BOLD, 38));
        labelTime.setFont(new Font("SansSerif", Font.BOLD, 38));

        Button buttonBack = new Button("Zurück");
        buttonBack.addActionListener((ActionEvent e) -> logic.back());

        Button buttonHelp = new Button("Hilfe");
        buttonHelp.addActionListener((ActionEvent e) -> logic.help());

        topPanel.setBackground(new Color(0, 255, 0));
        topPanel.add(buttonBack);
        topPanel.add(buttonHelp);
        topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));
        topPanel.add(labelSearch);
        topPanel.add(Box.createHorizontalGlue());
        topPanel.add(labelMissing);
        topPanel.add(labelTime);

        // init search field panel
        contentPanel = new JPanel();

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(topPanel);
        add(contentPanel);
    }

    /**
     * Fills the search field with given content.
     * @param content Content of search field.
     */
    public void initContent(String[][] content) {
        contentPanel.removeAll();
        contentPanel.setLayout(new GridLayout(content.length, content[0].length));
        this.content = new ContentLabel[content.length][content[0].length];
        for (int i = 0; i < content.length; i++) {
            for (int k = 0; k < content[0].length; k++) {
                this.content[i][k] = new ContentLabel(content[i][k], (VisualScan) logic, i, k);
                contentPanel.add(this.content[i][k]);
            }
        }
    }

    public int getContentHeight() {
        return contentPanel.getHeight();
    }

    public int getContentWidth() {
        return contentPanel.getWidth();
    }

    public void set(String label, int x, int y) {
        content[x][y].setText(label);
    }

    public void setSearch(String s) {
        labelSearch.setText(s);
    }

    public void setMissing(String s) {
        labelMissing.setText(s);
    }

    public void setTime(String s) {
        labelTime.setText(s);
    }

    public void setTimeLimitExceeded(boolean b) {
        labelTime.setForeground(b ? Color.RED : Color.BLACK);
    }

    public void setFontSize(int fontSize) {this.fontSize = fontSize; }

    /**
     * Applies function on all content label.
     * @param op Operator to be applied.
     */
    public void apply(UnaryOperator<JLabel> op) {
        for (JLabel[] arr : content) {
            for (JLabel label : arr) {
                op.apply(label);
            }
        }
    }
}
