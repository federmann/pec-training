package logic.game;

import gui.VisualScanPanel;
import logic.LogicUnit;
import logic.menu.VisualScanMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.image.BufferedImage;
import java.util.Random;
import java.util.function.IntUnaryOperator;
import java.util.function.LongBinaryOperator;
import java.util.function.UnaryOperator;

/**
 * Logic for game visual scan.
 */
public class VisualScan extends LogicUnit {
    /**
     * Symbols for search.
     */
    public enum Symbols {
        NUMBERS("Zahlen", '0', '9'), LETTERS_LOWER("Kleinbuchstaben", 'a', 'z'), LETTERS_UPPER("Großbuchstaben", 'A', 'Z');

        /**
         * String representation of symbols.
         */
        private final String text;
        private final char start, end;

        /**
         * Default constructor.
         *
         * @param text  String representation of symbols.
         * @param start First character of used symbols (inclusive).
         * @param end   Last character of used symbols (inclusive).
         */
        Symbols(String text, char start, char end) {
            this.text = text;
            this.start = start;
            this.end = end;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    /**
     * Number of rows.
     */
    private final int rows;
    /**
     * Number of columns.
     */
    private final int columns;
    /**
     * Percentage of whitespace.
     */
    private final float whitespace;
    /**
     * Total number of missing symbols as function of missing symbols and level to time limit in milliseconds.
     */
    private final int missing_total;
    /**
     * Time limit to find all missing symbols.
     */
    private final LongBinaryOperator timeLimit;

    /**
     * Character to be found.
     */
    private char find;
    /**
     * Time limit in milliseconds.
     */
    private long timeLimitMillis;
    /**
     * Currently missing symbols.
     */
    private int missing;
    /**
     * Level of the game; determines time limit.
     */
    private int level;
    /**
     * System time at game start.
     */
    private long start_time;
    /**
     * Representation of search field.
     */
    private String[][] content;
    /**
     * Thread to update in game timer.
     */
    private Thread timer;
    /**
     * Font size.
     */
    private int fontSize;

    /**
     * Symbols for search.
     */
    private final Symbols symbols;
    private final VisualScanPanel ui;

    /**
     * Creates a visual scan game with scaling size.
     *
     * @param context              Context of ui.
     * @param missingPercentage    Percentage of symbols to find.
     * @param whitespacePercentage Percentage of empty fields.
     * @param timeLimit            Time limit as function from missing symbols and level to time limit in milliseconds.
     * @param level                Level of game.
     * @param symbols              Symbols for search.
     */
    public VisualScan(Container context, float missingPercentage, float whitespacePercentage, LongBinaryOperator timeLimit, int level, Symbols symbols) {
        super(context);

        // create ui
        ui = new VisualScanPanel(this);
        loadUI(ui);

        // transfer values

        this.fontSize = 38;
        ui.setFontSize(this.fontSize);
        whitespacePercentage = Math.min(whitespacePercentage, 100.0f);
        whitespacePercentage = Math.max(whitespacePercentage, .0f);
        this.whitespace = whitespacePercentage;
        this.symbols = symbols;
        this.columns = calculateColumns(symbols);
        this.rows = calculateRows(symbols);
        missingPercentage = Math.min(missingPercentage, 100.0f);
        missingPercentage = Math.max(missingPercentage, .0f);
        this.missing = (int) (missingPercentage * columns * rows) / 100;
        this.missing_total = this.missing;
        this.level = level;
        this.timeLimit = timeLimit;
        this.timeLimitMillis = timeLimit.applyAsLong(this.missing_total, level);
        content = generate(symbols);
        ui.initContent(content);
        ui.setSearch("Suche: " + find);
        ui.setMissing("(" + this.missing + " übrig)");

        start_time = System.currentTimeMillis();

        startTimer();
    }

    /**
     * Creates a visual scan game with static size.
     *
     * @param context              Context of ui.
     * @param missing              Number of symbols to find.
     * @param whitespacePercentage Percentage of empty fields.
     * @param timeLimit            Time limit as function from missing symbols and level to time limit in milliseconds.
     * @param level                Level of game.
     * @param symbols              Symbols for search.
     */
    public VisualScan(Container context, IntUnaryOperator missing, int rows, int columns, float whitespacePercentage, LongBinaryOperator timeLimit, int level, Symbols symbols) {
        super(context);

        ui = new VisualScanPanel(this);
        loadUI(ui);
        this.rows = rows;
        this.columns = columns;
        this.fontSize = calculateFontSize(symbols);
        ui.setFontSize(fontSize);
        this.level = level;
        whitespacePercentage = Math.min(whitespacePercentage, 100.0f);
        whitespacePercentage = Math.max(whitespacePercentage, .0f);
        this.missing = Math.min(missing.applyAsInt(level), rows * columns);
        this.missing_total = this.missing;
        this.whitespace = whitespacePercentage;
        this.symbols = symbols;
        this.timeLimit = timeLimit;
        this.timeLimitMillis = timeLimit.applyAsLong(this.missing_total, level);

        content = generate(symbols);
        ui.initContent(content);
        ui.setSearch("Suche: " + find);
        ui.setMissing("(" + this.missing + " übrig)");

        start_time = System.currentTimeMillis();

        startTimer();
    }

    private int calculateFontSize(Symbols symbols) {
        int s;
        int h = ui.getContentHeight() - 50, w = ui.getContentWidth(), h_ = Integer.MIN_VALUE, w_ = Integer.MIN_VALUE;
        FontRenderContext fontRenderContext = context.getGraphics().getFontMetrics().getFontRenderContext();
        for (s = 0; h_ * rows <= h && w_ * columns <= w; ++s) { // find biggest possible font.
            // find largest symbol
            for (char c = symbols.start; c <= symbols.end; c++) {
                h_ = Math.max((int) new Font("SansSerif", Font.BOLD, s).getStringBounds("" + c, fontRenderContext).getHeight(), h_);
                w_ = Math.max((int) new Font("SansSerif", Font.BOLD, s).getStringBounds("" + c, fontRenderContext).getWidth(), w_);
            }
        }
        return s - 2; // s gets incremented after already being too big -> reduce by 2.
    }

    /**
     * Calculates number of rows according to interface height and symbol height.
     *
     * @param symbols Symbols for search.
     * @return Number of rows.
     */
    private int calculateRows(Symbols symbols) {
        int h = ui.getContentHeight(), i = Integer.MAX_VALUE;
        // Find tallest symbol
        FontRenderContext fontRenderContext = context.getGraphics().getFontMetrics().getFontRenderContext();
        for (char c = symbols.start; c <= symbols.end; c++) {
            i = Math.min((int) (h / new Font("SansSerif", Font.BOLD, fontSize).getStringBounds("" + c, fontRenderContext).getHeight()), i);
        }
        return i;
    }

    /**
     * Calculates number of columns according to interface width and symbol width.
     *
     * @param symbols Symbols for search.
     * @return Number of columns.
     */
    private int calculateColumns(Symbols symbols) {
        int w = ui.getContentWidth(), i = Integer.MAX_VALUE;
        // Find tallest symbol.
        FontRenderContext fontRenderContext = context.getGraphics().getFontMetrics().getFontRenderContext();
        for (char c = symbols.start; c <= symbols.end; c++) {
            i = Math.min((int) (w / new Font("SansSerif", Font.BOLD, fontSize).getStringBounds("" + c, fontRenderContext).getWidth()), i);
        }
        return i;
    }

    /**
     * Starts in game timer.
     */
    private void startTimer() {
        if (timer != null && timer.isAlive())
            return;
        timer = new Thread(() -> {
            while (true) {
                long time = System.currentTimeMillis() - start_time;
                ui.setTimeLimitExceeded(time > timeLimitMillis);
                ui.setTime("Zeit: " + parseTime(time));
                if (time > timeLimitMillis + 60000) {
                    new Thread(() -> {
                        end();
                    }).start();
                }
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    break;
                }
            }
        });
        timer.start();
    }

    /**
     * Generates search field.
     *
     * @param symbols Symbols for search.
     * @return Search field as 2-dimensional array of strings.
     */
    private String[][] generate(Symbols symbols) {
        int range = symbols.end - symbols.start;

        String[][] out = new String[rows][columns];
        Random r = new Random();
        // get random symbol to find
        find = (char) (symbols.start + r.nextInt(range + 1));
        for (int i = 0; i < rows; i++) {
            for (int k = 0; k < columns; k++) {
                char c;
                if (Math.random() < whitespace) { // empty field
                    c = ' ';
                } else {
                    c = (char) (symbols.start + r.nextInt(range)); // create random symbol
                    if (c >= find) // symbol to find is inserted later
                        c++;
                }
                out[i][k] = "" + c;
            }
        }
        for (int i = 0; i < missing; i++) { // generating symbols to find
            int pos = r.nextInt(rows * columns);
            // ignore fields that already contain searched symbol
            while (!out[pos / columns][pos % columns].isEmpty() && out[pos / columns][pos % columns].charAt(0) == find) {
                pos = r.nextInt(rows * columns);
            }
            out[pos / columns][pos % columns] = "" + find;
        }
        return out;
    }

    /**
     * Restarts the game with given level. All other values stay the same.
     *
     * @param level Level of game.
     */
    private void restart(int level) {
        this.level = level;
        this.timeLimitMillis = timeLimit.applyAsLong(missing_total, level);
        missing = missing_total;
        content = generate(symbols);
        ui.apply((JLabel l) -> {
            l.setForeground(Color.black);
            return l;
        });
        for (int i = 0; i < rows; i++) {
            for (int k = 0; k < columns; k++) {
                ui.set(content[i][k], i, k);
            }
        }
        ui.setSearch("Suche: " + find);
        ui.setMissing("(" + missing + " übrig)");
        start_time = System.currentTimeMillis();
        startTimer();
    }

    /**
     * Clicks on field with given ooordinates.
     *
     * @param x x-coordinate.
     * @param y y-coordinate.
     */
    public void click(int x, int y) {
        // field contains searched symbol
        if (!content[x][y].isEmpty() && content[x][y].charAt(0) == find) {
            content[x][y] = " "; // remove symbol
            missing--; // update missing symbols
            // update ui
            ui.set(" ", x, y);
            ui.setMissing("(" + missing + " übrig)");
        }
        if (missing <= 0) { // end game if needed
            end();
        }
    }

    /**
     * Ends the game.
     */
    private void end() {
        timer.interrupt(); // end timer
        long finish_time = System.currentTimeMillis();
        long time = finish_time - start_time; // calculate used time
        // End screen
        Object[] options = {"Neu starten", "Zurück zum Menü"};
        ImageIcon icon = new ImageIcon(getClass().getResource(time <= timeLimitMillis ? "/smiley.png" : "/smiley_sad.png"));
        BufferedImage in = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics g = in.createGraphics();
        // paint the Icon to the BufferedImage.
        icon.paintIcon(null, g, 0, 0);
        g.dispose();

        icon = new ImageIcon(new ImageIcon(in).getImage().getScaledInstance(100, 100, 0));

        String msg = "Zeit: " + parseTime(time);
        if (missing > 0) {
            msg += "\nZeitlimit mehr als 1 Minute überschritten :(";
        } else if (timeLimitMillis >= 0) {
            msg += "\n" + parseTime(Math.abs(time - timeLimitMillis));
            if (time < timeLimitMillis) {
               msg += " unter dem Zeitlimit :)";
            } else {
               msg += " über dem Zeitlimit :(";
            }
        }
        switch (JOptionPane.showOptionDialog(null, msg, "Fertig", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon, options, options[0])) {
            case JOptionPane.YES_OPTION:
                restart(level);
                break;
            case JOptionPane.NO_OPTION:
                back();
                break;
        }
    }

    /**
     * Returns to game menu.
     */
    public void back() {
        context.remove(ui);
        new VisualScanMenu(context);
    }

    public void applyMissing(UnaryOperator<JLabel> op) {
        ui.apply((JLabel l) -> {
            if (l.getText().charAt(0) == find) op.apply(l);
            return l;
        });
    }

    public void help() {
        applyMissing((JLabel l) -> {
            Color colorHighlight = Color.red;
            if (l.getForeground().equals(colorHighlight)) {
                l.setForeground(Color.black);
            } else {
                l.setForeground(colorHighlight);
            }
            return l;
        });
    }

}
