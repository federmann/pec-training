package logic;

import gui.GUIUnit;

import java.awt.*;

/**
 * Superclass for all logic parts.
 */
public class LogicUnit {
    protected GUIUnit ui;
    protected final Container context;

    protected LogicUnit(Container context) {
        this.context = context;
    }

    protected void loadUI(GUIUnit ui) {
        this.ui = ui;
        context.add(ui);
        context.setVisible(true);
    }

    /**
     * Parses given time to string.
     * @param time Time in milliseconds.
     * @return Time in hh:mm:ss format.
     */
    protected String parseTime(long time) {
        long min, sec;
        min = time / 60000;
        time -= min * 60000;
        sec = time / 1000;
        time -= sec * 1000;
        return min + ":" + (sec < 10 ? "0" : "")  + sec + ":" + (time < 100 ? "0" : "") + (time < 10 ? "0" : "") + time;
    }
}
