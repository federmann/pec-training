package logic.menu;

import gui.MenuPanel;
import logic.LogicUnit;

import java.awt.*;

/**
 * Logic for Main Menu.
 */
public class Menu extends LogicUnit {

    public Menu(Container context) {
        super(context);
        ui = new MenuPanel(this);
        loadUI(ui);
    }

    /**
     * Starts the game.
     */
    public void startGame() {
        context.remove(ui);
        new VisualScanMenu(context);
    }

    /**
     * Exits the game.
     */
    public void exit() {
        System.exit(0);
    }
}
