package logic.menu;

import gui.VisualScanMenuPanel;
import logic.LogicUnit;
import logic.game.VisualScan;

import java.awt.*;

/**
 * Logic for menu of game visual scan.
 */
public class VisualScanMenu extends LogicUnit {

    public VisualScanMenu(Container context) {
        super(context);
        ui = new VisualScanMenuPanel(this);
        loadUI(ui);
    }

    /**
     * Returns to main menu.
     */
    public void back() {
        context.remove(ui);
        new Menu(context);
    }

    /**
     * Starts the game.
     * @param symbols Symbols for search.
     * @param level Level to determine expected time. From 1 to 3.
     */
    public void start(VisualScan.Symbols symbols, int level) {
        context.remove(ui);
        //new VisualScan(context, 3.5f, .6f, (long missing, long lvl) -> missing * (2000 + (3 - level) * 200), level, symbols);
        new VisualScan(context, (int lvl) -> (lvl == 1 ? 45 : 55), 30, 60, .6f, (long missing, long lvl) -> (lvl == 1 ? 150000 : 165000), level, symbols);
        //new VisualScan(context, (int lvl) -> 1, 2, 2, .0f, (long missing, long lvl) -> missing * (2000 + (3 - level) * 200), level, symbols);
    }
}
