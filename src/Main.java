import logic.menu.Menu;

import javax.swing.*;

public class Main {
    public static void main(String arg[]) {
        JFrame frame = new JFrame("PEC Training");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH); // Fullscreen
        frame.setUndecorated(true); // Fullscreen
        new Menu(frame);
    }
}
